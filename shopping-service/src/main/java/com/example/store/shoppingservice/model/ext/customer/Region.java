package com.example.store.shoppingservice.model.ext.customer;

import lombok.Data;

@Data
public class Region {
    private Long id;
    private String name;
}
