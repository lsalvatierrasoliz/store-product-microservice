package com.example.store.shoppingservice.client;

import com.example.store.shoppingservice.model.ext.customer.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "customer-service", fallback = CustomerClient.CustomerFallbackFactory.class)
public interface CustomerClient {

    @GetMapping("/store/v1/customers/{customerId}")
    ResponseEntity<Customer> getById(@PathVariable(name = "customerId") Long customerId);

    @Component
    class CustomerFallbackFactory implements CustomerClient {
        @Override
        public ResponseEntity<Customer> getById(Long customerId) {
            Customer customer = Customer.builder()
                    .firstName("none")
                    .lastName("none")
                    .email("none")
                    .build();

            return ResponseEntity.ok(customer);
        }
    }
}
