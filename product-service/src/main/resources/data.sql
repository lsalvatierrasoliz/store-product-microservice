INSERT INTO tbl_categories (id, name) VALUES (1, 'shoes');
INSERT INTO tbl_categories (id, name) VALUES (2, 'books');
INSERT INTO tbl_categories (id, name) VALUES (3, 'electronics');

INSERT INTO tbl_products (id, name, description, stock, price, status, create_at, category_id)
VALUES (1, 'adidas', 'walk any place', 5, 250.0, 'CREATED', '2018-09-05', 1);

INSERT INTO tbl_products (id, name, description, stock, price, status, create_at, category_id)
VALUES (2, 'La Iliada', 'Novela', 3, 80.0, 'CREATED', '2018-09-05', 2);

INSERT INTO tbl_products (id, name, description, stock, price, status, create_at, category_id)
VALUES (3, 'TV samsumg', 'Smart TV', 10, 1400.0, 'CREATED', '2018-09-05', 3);