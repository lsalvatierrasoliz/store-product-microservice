package com.example.store.product.rest;

import com.example.store.product.entity.Product;
import com.example.store.product.model.product.PageProductResponse;
import com.example.store.product.model.product.ProductUpdateStockRequest;
import com.example.store.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping(value = "/store/v1/products")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<PageProductResponse> listAll(@RequestParam(value = "status", required = false) String status,
                                                       @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                                                       @RequestParam(value = "size", required = false, defaultValue = "50") int size) {

        PageProductResponse products = productService.listAll(Optional.ofNullable(status), page, size);
        if (products.getTotalItems() == 0) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(products);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<Product> getById(@PathVariable("productId") Long productId) {
        Product getProduct = productService.getById(productId);
        return ResponseEntity.ok(getProduct);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody @Valid Product product) {
        Product productAdded = productService.create(product);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productAdded.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{productId}")
    public ResponseEntity<Product> update(@PathVariable("productId") Long productId, @RequestBody Product product) {
        product.setId(productId);
        Product productUpdate = productService.update(product);

        return ResponseEntity.ok(productUpdate);
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<?> deleteById(@PathVariable("productId") Long productId) {
        productService.deleteById(productId);

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{productId}/update-stock")
    public ResponseEntity<Product> updateStock(@PathVariable("productId") Long productId,
                                               @RequestBody ProductUpdateStockRequest updateStockRequest) {

        Product productStockUpdate = productService.updateStock(productId, updateStockRequest.getQuantity());
        return ResponseEntity.ok(productStockUpdate);

    }


}
