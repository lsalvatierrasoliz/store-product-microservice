package com.example.store.product.model.exception;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 8845841090708272579L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
