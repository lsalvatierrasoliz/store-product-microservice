package com.example.store.customer.service;

import com.example.store.customer.entity.Customer;
import com.example.store.customer.entity.Region;
import com.example.store.customer.model.customer.PageCustomerResponse;

import java.util.List;


public interface CustomerService {
    Customer create(Customer customer);

    Customer update(Customer customer);

    Customer getById(Long id);

    PageCustomerResponse listAll(int page, int size);

    PageCustomerResponse findByRegion(Region region, int page, int size);

    Customer deleteById(Long id);
}
