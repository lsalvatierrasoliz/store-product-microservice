package com.example.store.customer.repo;

import com.example.store.customer.entity.Customer;
import com.example.store.customer.entity.Region;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByNumberID(String numberID);

    Page<Customer> findByRegion(Region region, Pageable pageable);

    Optional<Customer> findByLastName(String lastName);
}
