package com.example.store.customer.model.exception;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -1425014165275644678L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
